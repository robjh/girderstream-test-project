# A basic Girderstream project

This project uses freedesktop-sdk as its too chain and produces minimal systems

We currently have two targets but nether is very complete, the kernel has
been show to work on a pi3 and the busy box to work on x86.

## Selecting target

We use a girderstream option to pick the target ie

```
girderstream --options "bsp=pi"  --verbose build --name busybox
```

or

```
girderstream --options "bsp=x86"  --verbose build --name busybox
```


## Local cas

Currently you need a local cas running and you must do this your self

For example

```
buildbox-casd  --bind localhost:50040 ~/.cache/girderstream/cas1
buildbox-casd  --bind localhost:50040 ~/.cache/girderstream/cas44
```

## Using the project

You can check the state of a build with

```
girderstream --verbose show --name kernel
```

You can build a element like

```
girderstream --options bsp=x86 build --name filesystem
```

And checkout with

``` bash
girderstream --options bsp=x86 checkout --name kernel --location linux_x86
girderstream --options bsp=x86 checkout --name filesystem --location filesystem_x86
```

or

``` bash
girderstream --options bsp=aarch64 checkout --name kernel --location linux_aarch64
girderstream --options bsp=aarch64 checkout --name filesystem --location filesystem_aarch64
```

or

``` bash
girderstream --options bsp=pi checkout --name filesystem --location filesystem_pi
```

But please note that there are some known issues with girderstream checkout
and some of the links that the kernel uses so you might be better with buildroots
casdownload utility.

## Girderstream Compatiablity

Girderstream is in active development, at the last check 79bcbf3 of girderstream
is required to use this project.


## Running

This works for the x86 bsp

``` bash
qemu-system-x86_64 -nographic -drive file=filesystem_x86/disk.img,format=raw -kernel linux_x86/boot/bzImage -append "earlyprintk=serial,ttyS0 console=ttyS0 root=/dev/sda2 rw"

qemu-system-x86_64 -drive file=filesystem_x86/disk.img,format=raw -kernel linux_x86/boot/bzImage -append "earlyprintk=serial,ttyS0 console=ttyS0 root=/dev/sda2 rw"
```

```
/ # /usr/local/bin/hello
This is amhello 1.0.
```

The following will boot the aarch64 image

```
qemu-system-aarch64 -machine virt -cpu cortex-a57 -machine type=virt -nographic -smp 2 -m 4096 -kernel linux_aarch64/boot/Image -drive file=filesystem_aarch64/disk.img,format=raw -append "root=/dev/vda2 rw"
```

To boot with a video output setup you can use:

```
qemu-system-aarch64 -device virtio-gpu-pci -machine virt -cpu cortex-a57 -machine type=virt -smp 2 -m 4096 -kernel linux_aarch64/boot/Image -drive file=filesystem_aarch64/disk.img,format=raw -append "root=/dev/vda2 rw"
```

Inside the running qemu you can then run

```
/ # modprobe virtio_gpu
/ # modprobe vivid
/ # modprobe drm
```

And then you will see the virtio screen turn on and have some text from the kernel.

```
/ # cam --camera 1 -C -D
[0:13:53.742593680] [267]  INFO Camera camera_manager.cpp:299 libcamera v0.0.4
[0:13:53.857987264] [268]  WARN CameraSensorProperties camera_sensor_properties.cpp:231 No static properties available for 'Sensor B'
[0:13:53.859118128] [268]  WARN CameraSensorProperties camera_sensor_properties.cpp:233 Please consider updating the camera sensor properties database
[0:13:53.859943872] [268]  WARN CameraSensor camera_sensor.cpp:459 'Sensor B': Failed to retrieve the camera location
Using camera platform/vimc.0 Sensor B as cam0
[0:13:53.924722128] [267]  INFO Camera camera.cpp:1028 configuring streams: (0) 1920x1080-BGR888
Unable to find display pipeline for format BGR888
Failed to configure frame sink
Failed to start camera session
```

For the pi you should write to a sd card like:

```
sudo dd bs=64k status=progress of=/dev/sdX if=filesystem_pi/disk.img
```

Changing sdX to point to your SD card and taking care not to point to
anything important.

And then you can use the sd card to boot the pi.

Once you boot the pi you can then run 

```
cam --camera 1 -C -D
```

And the screen will be filled with a camera preview
