kind: genimage

provides: filesystem

depends:
- dependency: busybox
  scope: Build
  stage: Run
  location: /targetfs/
- dependency: kernel
  scope: Build
  stage: Run
  location: /targetfs/
- dependency: firmware
  scope: Build
  stage: Run
  location: /targetfs/
- dependency: demo_app
  scope: Build
  stage: Run
  location: /targetfs/
- dependency: base_libs
  scope: Build
  stage: Run
  location: /targetfs/
- dependency: libcamera
  location: /targetfs/
  scope: Build
  stage: Run
- dependency: libcamera-rs
  location: /targetfs/
  scope: Build
  stage: Run
- dependency: v4l-utils
  scope: Build
  location: /targetfs/
  stage: Run

sources: []



plugin_variables:
  join_strategy: Join
  variables:
    host-arch: x86_64

    # this is equivalent to bst's
    # config:
    # - install-commands
    #
    # /girderstream_build_plugin_bst takes all the config_XYZ from the plugin
    # and combins them with the plugin_type.yaml to create ./configured.sh
    config_install-commands: |
      cat >/targetfs/boot/cmdline.txt <<EOF
      console=serial0,115200 console=tty1 root=/dev/mmcblk0p2 rw rootfstype=ext4 rootwait
      EOF

      cat >/targetfs/boot/config.txt <<EOF
      # Run in 64-bit mode
      arm_64bit=1
      start_file=start4.elf
      fixup_file=fixup4.dat
      camera_auto_detect=0
      displpay_auto_detect=1
      dtoverlay=vc4-kms-v3d
      dtoverlay=imx219
      enable_uart=1
      max_framebuffers=2
      gpu_mem=128
      EOF

      mkdir -p /targetfs/etc/
      echo "root:x:0:0:root:/root:/bin/bash" > /targetfs/etc/passwd
      cat >/targetfs/etc/inittab <<EOF
      ::sysinit:/etc/init.d/rcS
      ::askfirst:-/bin/sh
      tty1::respawn:/sbin/getty  -L 115200 -l /bin/login ttyS0
      ttyAMA0::respawn:/sbin/getty -L 115200 -l /bin/login ttyS1
      EOF

      mkdir /genimage/
      cat >/genimage/genimage.cfg <<EOF
      image boot.img {
        vfat {
            label = "BOOT"
        }
        mountpoint = "/boot/"
        size = 300M
      }
      image root.img {
          ext4  {
              label = "ROOT"
              extraargs = "-E no_copy_xattrs"
              use-mke2fs = true
          }
          size = 8000M
      }
      image disk.img {
        hdimage {
            align = 1M
        }
        partition boot {
            partition-type = 0xC
            bootable = "true"
            image = "boot.img"
        }
        partition root {
            partition-type = 0x83
            image = "root.img"
        }
      }
      EOF
      cd /genimage/

      ls -la /targetfs/
      echo "ls /targetfs/boot"
      ls -la /targetfs/boot

      find /targetfs/ -name "lto1.debug"
      rm -rf /targetfs/usr/lib/debug/
      #exit 1

      cat <<EOF >/targetfs/etc/mdev.conf
      \$MODALIAS=.* root:root 0660 @modprobe -b "\$MODALIAS"
      EOF

      cat <<EOF >/targetfs/etc/modules
      vimc
      rfcomm
      cmac
      algif_hash
      aes_arm64
      algif_skcipher
      af_alg
      bnep
      hci_uart
      btbcm
      bluetooth
      ecdh_generic
      ecc
      8021q
      garp
      stp
      llc
      imx219
      snd_soc_hdmi_codec
      joydev
      vc4
      cec
      drm_kms_helper
      snd_soc_core
      snd_compress
      snd_pcm_dmaengine
      syscopyarea
      sysfillrect
      brcmfmac
      sysimgblt
      fb_sys_fops
      brcmutil
      i2c_mux_pinctrl
      i2c_mux
      raspberrypi_hwmon
      cfg80211
      bcm2835_unicam
      rfkill
      v4l2_dv_timings
      v4l2_fwnode
      v4l2_async
      bcm2835_codec
      bcm2835_isp
      bcm2835_v4l2
      v4l2_mem2mem
      bcm2835_mmal_vchiq
      videobuf2_vmalloc
      videobuf2_dma_contig
      videobuf2_memops
      videobuf2_v4l2
      snd_bcm2835
      videobuf2_common
      snd_pcm
      videodev
      snd_timer
      i2c_bcm2835
      mc
      snd
      vc_sm_cma
      uio_pdrv_genirq
      uio
      i2c_dev
      drm
      drm_panel_orientation_quirks
      backlight
      fuse
      ip_tables
      x_tables
      ipv6
      EOF

      mkdir -p /targetfs/etc/init.d/
      cat <<EOF >/targetfs/etc/init.d/rcS
      #!/bin/sh
      export PATH=$PATH:"%{bindir}/%{target-triplet}"/

      mkdir -p /proc
      mkdir -p /sys
      mkdir -p /dev
      mkdir -p /boot
      mount -t tmpfs -o size=64k,mode=0755 tmpfs /dev
      mkdir /dev/pts
      mount -t devpts devpts /dev/pts

      mount -t proc p /proc
      #mount -t sysfs none /sys
      mount -t sysfs sysfs /sys
      mount -t debugfs none /sys/kernel/debug
      #mount -t devtmpfs none /dev
      #mount -t proc proc /proc

      #depmod
      echo /sbin/mdev > /proc/sys/kernel/hotplug
      mdev -s
      #grep -h MODALIAS /sys/bus/*/devices/*/uevent | cut -d = -f 2 | sort -u | xargs modprobe -ab
      #cat /etc/modules | xargs modprobe -ab

      mount /dev/mmcblk0p1 /boot
      LD_LIBRARY_PATH=/usr/lib/%{target-triplet}/
      LD_LIBRARY_PATH=/usr/lib/%{base-target-triplet}/:/usr/lib/%{target-triplet}/
      export LD_LIBRARY_PATH
      #depmod
      #ttyS0::respawn:/bin/sh
      exec /bin/sh
      EOF
      chmod a+x /targetfs/etc/init.d/rcS

      genimage --rootpath /targetfs/
      cp images/disk.img /output_dir/



    # this bit should go in the plugin definition rather than here
    # but as this is a bit work in progress its ok here
    cli: |
      mkdir -p /sources/build
      mkdir -p /output_dir/
      cd /sources/
      girderstream_build_plugin_bst -v /plugin_vars.yaml -p /manual.yaml
      # the /plugin_vars.yaml comes from girderstream and happens for all plugins
      # the /girderstream_build_plugin_bst and /manual.yaml come from the plugin.
      # The plugin also provides cmake.yaml and autotools.yaml and they contain
      # the templates for thier equivalent bst build plugins.
      # You can use any bst build plugin that is just yaml in this way, eg:
      # /girderstream_build_plugin_bst -v /plugin_vars.yaml -p /any_bst_plugin_yaml.yaml
      #
      # The file /plugin_vars.yaml comes from girderstream and contains all the
      # other variables from the project etc, /girderstream_build_plugin_bst then
      # does the variable substitution that buildstream would do in the plugin
      # base class.
      #
      # Once they are combined it creates ./configured.sh which we can run
      # to get the same effect as any bst build plugin that just used .yaml build.
      # Plugins that have more than the basic .py file will need more converting
      # to Girderstream.
      chmod u+x configured.sh
      cat configured.sh
      ./configured.sh
